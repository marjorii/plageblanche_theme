# Plage blanche Theme

The **Plage blanche** Theme is for [Grav CMS](http://github.com/getgrav/grav).

## Description

Custom theme for Plage blanche.
Apply to plageblanche_skeleton.

## Licence

Copyright (C) 2019 Marjorie Ober

This program is free software: you can redistribute it and/or modify  
it under the terms of the GNU General Public License as published by  
the Free Software Foundation, either version 3 of the License, or  
(at your option) any later version.

This program is distributed in the hope that it will be useful,  
but WITHOUT ANY WARRANTY; without even the implied warranty of  
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the  
GNU General Public License for more details.

You should have received a copy of the GNU General Public License  
along with this program. If not, see [licenses](https://www.gnu.org/licenses/).
