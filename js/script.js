// INIT

const form = document.getElementById('form')
const leftSection = document.getElementById('left');
const rightSection = document.getElementById('right');

const player = document.getElementById('player');
const pageButtons = document.querySelectorAll('#participate .page-mode button');

const copyButton = document.createElement('span');
const dropzone = document.querySelector('.dropzone');

const grabIcon = document.getElementById('grab-icon');

// DROPZONE

// Make copyButton tabable and toggle dropzone click on enter key press
dropzone.appendChild(copyButton);
copyButton.setAttribute('tabindex', '0');
copyButton.onkeydown = function(e) {
    if ((e.keyCode === 13)) {
        dropzone.click();
    }
}
// Make copyButton clickable and toggle dropzone click on copyButton click
copyButton.onclick = function() {
    dropzone.click();
}


// ARIA-LABELS ON FORM BUTTONS

// Dropzone, color and submit buttons
document.querySelector('.dropzone > span').setAttribute('role', 'button');

if (window.location.href.includes('fr')) {
    document.querySelector('.dropzone > span').setAttribute('title', "Insérer un fichier audio MP3 d'environ une minute");
    document.querySelector('input[type="color"]').setAttribute('title', 'Choisir une couleur');
    document.querySelector('#form button[type="submit"]').setAttribute('title', 'Envoyer le formulaire');
}
else {
    document.querySelector('.dropzone > span').setAttribute('title', 'Add an MP3 audio file of about one minute');
    document.querySelector('input[type="color"]').setAttribute('title', 'Choose a color');
    document.querySelector('#form button[type="submit"]').setAttribute('title', 'Submit form');
}

// MISSING TITLES ON FORM fields
let fieldNames = document.querySelectorAll('#form input[type="text"], #form textarea');
fieldNames.forEach((fieldName) => {
    fieldName.setAttribute('title', fieldName.getAttribute('placeholder'));
});




// DISABLE DOUBLE CONTENT

// Disable player intro links and nav menu buttons
document.querySelectorAll('#player .page-mode button').forEach((button) => {
    button.disabled = true;
});
document.querySelectorAll('#player .intro a, #player .lang a').forEach((a) => {
    a.setAttribute('tabindex', '-1');
    a.setAttribute('href', '');
});


// BUTTON LIKE ACTONS

// Toggle underline class for player about page link
buttonLike(document.querySelector('#participate .intro p a'), document.querySelector('#player .intro p a'), 'underline')

// Toggle selected class for player lang buttons
document.querySelectorAll('#participate .lang a').forEach((button) => {
    buttonLike(button, document.querySelector('#player .lang a[data-lang="' + button.dataset.lang + '"]'), 'focused');
});

// DURATION: set hours to minutes
document.querySelectorAll('section#right > div li li:last-of-type').forEach((dur) => {
    dur.textContent = '00:' + dur.textContent;
});




// CLICK EVENTS

// Nav buttons click events
pageButtons.forEach(function(button) {

    // Toggle selected class for player nav menu buttons
    buttonLike(button, document.querySelector('#player button[data-slug="' + button.dataset.slug + '"]'), 'focused');

    button.onclick = function(e) {
        // Handle selected class
        removePrevSelectedClass(document.querySelector('#participate .page-mode button.selected'));

        // CONTRIBUTORS, TITLES, COLORS, TAGS AND YEARS BUTTONS

        if (button.dataset.slug == 'contributors' || button.dataset.slug == 'titles' || button.dataset.slug == 'colors' || button.dataset.slug == 'tags' ||  button.dataset.slug == 'years') {
            // Handle player and right section size
            playerResize(10, 10);

            // Handle form, research and player inputs access
            inputAccess('-1', '0', '-1');

            // Handle selected class
            removePrevSelectedClass(document.querySelector('#participate .page-content button.selected'));

            // Handle corresponding container behaviour
            const prevContainer = document.querySelector('#right > div.current');
            if (prevContainer) {
                prevContainer.classList.remove('current');
            }
            const container = document.getElementById(button.dataset.slug);
            if (container.classList.contains('hide')) {
                container.classList.remove('hide');
            }
            if (!container.classList.contains('current')) {
                container.classList.add('current');
            }

            // Toggle parent button selected class
            addSelectedClass('button[data-slug="research"]');

            // Hide uncurrent containers
            const notCurrent = document.querySelectorAll('#right > div:not(.current)');
            for (var i = 0; i < notCurrent.length; i++) {
                if (!notCurrent[i].classList.contains('hide')) {
                    notCurrent[i].classList.add('hide');
                }
            }

            computeSectionRightSize()
        }


        // COMMON TO ALL BUTTONS

        // Handle form and right section accorging to device size and hash (on button click)
        removeSectionsOnclick(button);

        // Position grab icon
        setTimeout(() => {
            positionGrabIcon(grabIcon);
        }, 10);

        // Remove scrollbar width if scroll
        // setTimeout(() => {
        //     extraSpace();
        // }, 20);

        // RESEARCH BUTTON

        if (button.dataset.slug == 'research') {

            // Toggle Research submenu
            document.querySelectorAll('.page-content').forEach((submenu) => {
                submenu.classList.toggle('hide');
            });

            // Handle player and right section size
            playerResize(10, 10);

            // Handle player content position
            positionContent();

            // Handle form, research and player inputs access
            inputAccess('-1', '0', '-1');

            computeSectionRightSize()

            // Set aria-expanded to true
            let ariaExpanded = button.getAttribute('aria-expanded') === 'true';
            button.setAttribute('aria-expanded', !ariaExpanded)

            // Add corresponding page hash and toggle selected class
            addSelectedClass('button[data-slug="research"]');
            window.location.hash = button.nextElementSibling.querySelector('button.selected').dataset.slug;

            // Handle focus so hash change doesn't move focus back to top of page
            if (!ariaExpanded) {
                button.nextElementSibling.querySelector('button.selected').focus()
            }
            else {
                button.focus();
            }
            return;
        }


        // PARTICIPATE AND PLAYER (PAGE) BUTTONS

        else if (button.dataset.slug == 'contribute' || button.dataset.slug == 'listen') {


            // PARTICIPATE BUTTON

            if (button.dataset.slug == 'contribute') {

                // Handle player and right section size
                requestAnimationFrame(() => {
                    playerResize(document.querySelector('section#left > .intro-container').clientHeight - (grabIcon.dataset.padding * 2), window.innerWidth - document.querySelector('section#left').clientWidth);
                });

                // Handle form, research and player inputs access
                inputAccess('0', '-1', '-1');
            }


            // PLAYER (PAGE) BUTTON

            if (button.dataset.slug == 'listen') {
                // Handle player and right section size
                playerFullScreen();

                // Handle form, research and player inputs access
                inputAccess('-1', '-1', '0');
            }


            // COMMON TO PARTICIPATE AND PLAYER BUTTONS
            button.focus()

            // Close Research submenu if another page button is clicked
            document.querySelectorAll('.page-content').forEach((submenu) => {
                submenu.classList.add('hide');
            });

            // Set aria-expanded to false
            document.querySelector('#participate .page-mode button[data-slug="research"]').setAttribute('aria-expanded', 'false');
        }


        // COMMON TO ALL BUTTONS EXCEPT RESEARCH

        // Change hash
        window.location.hash.replace('#', '');
        window.location.hash = button.dataset.slug;

        // Handle focus so hash change doesn't move focus back to top of page
        button.focus();

        // Active or disable pagination buttons (see pagination.js)
        paginationSwitch();

        // Handle selected class
        addSelectedClass('button[data-slug="' + button.dataset.slug + '"]');

        // Handle player content position
        positionContent();

    }
});

const whatDiv = document.getElementById('what-div');
const whatButton = document.querySelector('.what-button')

// Toggle text div on '?' button click
whatButton.onclick = () => {
    whatDiv.classList.toggle('hide');
    if (!whatDiv.classList.contains('hide')) {
        document.querySelector('#what-div h2:first-of-type').focus();
    }
    else {
        whatButton.focus();
    }
}

// Close text div on 'x' button click
whatDiv.querySelector('.cross').addEventListener('click', () => {
        whatDiv.classList.add('hide');
        whatButton.focus();
});

// Close text div with ESC
document.addEventListener("keydown", (e) => {
    if (!whatDiv.classList.contains('hide')) {
        if (e.keyCode === 27) {
            whatDiv.classList.add('hide');
            whatButton.focus();
        }
    }
}, false);




// WINDOW ON LOAD EVENTS

window.onload = (e) => {

    // PAGE HASH

    // Add mode to body
    onHashChanged();

    // Handle page hash and toggle corresponding behaviours and selected classes
    if (!window.location.hash) {
        window.location.hash = pageButtons[0].dataset.slug;
        pageButtons[0].click();
        addSelectedClass('button[data-slug="contributors"]');
    }
    else {
        if (window.location.hash == '#contribute' || window.location.hash == '#listen') {
            document.querySelector('button[data-slug="' + window.location.hash.replace('#', '') + '"]').click();
            addSelectedClass('button[data-slug="contributors"]');
        }
        else if (window.location.hash == '#contributors' || window.location.hash == '#titles' || window.location.hash == '#colors' || window.location.hash == '#tags' || window.location.hash == '#years') {
            document.querySelectorAll('.page-content').forEach((submenu) => {
                submenu.classList.remove('hide');
            });
            document.querySelector('button[data-slug="' + window.location.hash.replace('#', '') + '"]').click();
            // addSelectedClass('button[data-slug="contributors"]');
        }
        else {
            window.location.hash = pageButtons[0].dataset.slug;
            pageButtons[0].click();
            addSelectedClass('button[data-slug="contributors"]');
        }
    }

    // Handle player content position
    positionContent();

    computeSectionRightSize();

    setTimeout(() => {
        // Handle player and right section size
        if (window.location.hash == '#contribute') {
            playerResize(document.querySelector('section#left > .intro-container').clientHeight - (grabIcon.dataset.padding * 2), window.innerWidth - document.querySelector('section#left').clientWidth);
        }
        if (window.location.hash == '#listen') {
            playerFullScreen();
        }
        if (window.location.hash == '#contributors' || window.location.hash == '#titles' || window.location.hash == '#colors' || window.location.hash == '#tags' || window.location.hash == '#years') {
            playerResize(5, 5);
        }

        // Position grab icon
        positionGrabIcon(grabIcon);
    }, 10);

    // Remove scrollbar width if scroll
    // setTimeout(() => {
    //     extraSpace();
    // }, 20);

}


// WINDOW RESIZE EVENTS

// Handle form and right section accorging to device size and hash (on resize)
removeSectionsOnresize();
window.addEventListener('resize', () => {
    removeSectionsOnresize();
    // Handle player and right section size accorging to device size and hash
    setTimeout(() => {
        if (window.location.hash == '#contribute') {
            playerResize(document.querySelector('section#left > .intro-container').clientHeight - (grabIcon.dataset.padding * 2), window.innerWidth - document.querySelector('section#left').clientWidth);
        }
        if (window.location.hash == '#listen') {
            playerFullScreen();
        }
        if (window.location.hash == '#contributors' || window.location.hash == '#titles' || window.location.hash == '#colors' || window.location.hash == '#tags' || window.location.hash == '#years') {
            playerResize(5, 5);
        }
    }, 10);

    // Handle player content position
    positionContent();

    // Remove scrollbar width if scroll
    // setTimeout(() => {
    //     extraSpace();
    // }, 20);

    // Active or disable pagination buttons (see pagination.js)
    paginationSwitch();

    // Position grab icon
    positionGrabIcon(grabIcon);

    computeSectionRightSize();
});

// ORIENTATION CHANGE EVENTS

window.addEventListener("orientationchange", () => {
    setTimeout(() => {
        positionGrabIcon(grabIcon);
    }, 20);
    computeSectionRightSize()
});

// PLAYER RESIZE (MOUSE OR TOUCH) EVENTS

playerClientResize('touchstart', 'touchmove', 'touchend');
playerClientResize('mousedown', 'mousemove', 'mouseup');

setTimeout(() => {
    // Handle player and right section size
    if (window.location.hash == '#contribute') {
        playerResize(document.querySelector('section#left > .intro-container').clientHeight - (grabIcon.dataset.padding * 2), window.innerWidth - document.querySelector('section#left').clientWidth);
    }
    if (window.location.hash == '#listen') {
        playerFullScreen();
    }
    if (window.location.hash == '#contributors' || window.location.hash == '#titles' || window.location.hash == '#colors' || window.location.hash == '#tags' || window.location.hash == '#years') {
        playerResize(5, 5);
    }    // POSITION GRAB ICON
    positionGrabIcon(grabIcon);
}, 10);

// setTimeout(() => {
//     extraSpace();
// }, 20);


// UTILS

function addSelectedClass(buttons) {
    document.querySelectorAll(buttons).forEach((button) => {
        button.classList.add('selected');
    });
}

function removePrevSelectedClass(prev) {
    if (prev) {
        document.querySelectorAll('button[data-slug="' + prev.dataset.slug + '"]').forEach((prevButton) => {
            prevButton.classList.remove('selected');
        });
    }
}

function inputAccess(formValue, sectionValue, contentValue) {
    form.querySelectorAll('input, textarea, button, a, span').forEach((input) => {
        input.setAttribute('tabindex', formValue);
    });
    document.querySelectorAll('section#right button, section#right a').forEach((input) => {
        input.setAttribute('tabindex', sectionValue);
    });
    player.querySelector('#content a').setAttribute('tabindex', contentValue);
}

function buttonLike(button, elem, option) {
    button.onmouseover = button.onfocus = () => elem.classList.add(option);
    button.onmouseout = button.onblur = () => elem.classList.remove(option);
}

function onHashChanged () {
    document.body.dataset.mode = window.location.hash.replace('#', '')
}
window.addEventListener('hashchange', onHashChanged);

function computeSectionRightSize() {
    rightSection.style.setProperty('--section-width', rightSection.offsetWidth + "px");
}

// PLAYER CONTENT POSITION

function positionContentElements(content, ref) {
    content.style.top = ref.getBoundingClientRect().top + window.scrollY + 'px';
}

function positionContent() {
    const refs = document.querySelectorAll('form .form-field');
    const clones = document.querySelectorAll('#player #content > *');
    const offset = window.scrollY;
    for (let i = refs.length - 1; i >= 0; i--) {
        if (i === 4) {
            clones[3].style.top = refs[i].getBoundingClientRect().top + offset + 'px';
        } else if (i > 2) {
            clones[i - 1].style.top = refs[i].getBoundingClientRect().top + offset + 'px';
        } else {
            clones[i].style.top = refs[i].getBoundingClientRect().top + offset + 'px';
        }
    }
    const msg = clones[2];
    const top = refs[2].getBoundingClientRect().top;
    const bottom = refs[3].getBoundingClientRect().bottom;

    // set player content description height and width
    msg.style.height = bottom - top + 'px';
    msg.style.width = leftSection.offsetWidth + 'px';
}


// PLAYER RESIZE HANDLING

// mouse and touch events
function playerClientResize(start, move, end) {
    function onMove(e) {
        const { clientX, clientY } = move == 'touchmove' ? e.touches[0] : e;

        if (window.innerWidth < window.innerHeight) {
            player.style.height = window.scrollY + clientY + 5 + 'px';
        }
        else {
            player.style.width = window.innerWidth - clientX + 'px';
        }

        // Position grab icon
        positionGrabIcon(grabIcon);
    }

    function onEnd(e) {
        window.removeEventListener(move, onMove);
        window.removeEventListener(end, onEnd);
    }

    const grabzone = player.querySelector('#player-grab-zone');
    grabzone.addEventListener(start, (e) => {
        e.preventDefault();
        window.addEventListener(move, onMove);
        window.addEventListener(end, onEnd);
    });
    grabIcon.addEventListener(start, (e) => {
        e.preventDefault();
        window.addEventListener(move, onMove);
        window.addEventListener(end, onEnd);
    });


}

function positionGrabIcon(grabIcon) {
    if (window.innerWidth > window.innerHeight) {
        grabIcon.style.top = 'calc(50% - (' + grabIcon.dataset.padding * 2 + 'px))';
        grabIcon.style.transform = 'translateY(-50%)';
        grabIcon.style.left = (player.offsetLeft - 20) + 'px';
    }
    else {
        grabIcon.style.left = 'calc(50% - (' + grabIcon.dataset.padding * 2 + 'px))';
        grabIcon.style.transform = 'translateX(-50%) rotate(90deg)';
        grabIcon.style.top = (player.offsetHeight - 20) + 'px';
    }
}

// Click and resize events
function playerResize(varHeight, varWidth) {
    if (window.innerWidth < window.innerHeight) {
        player.style.width = window.innerWidth + 'px';
        player.style.height = varHeight + 'px';
    }
    else {
        player.style.height = window.innerHeight + 'px';
        rightSection.style.height = window.innerHeight + 'px';
        // if scroll needed, add scroll height to player and right section height
        if (document.firstElementChild.scrollHeight > document.firstElementChild.clientHeight) {
            const scrollHeight = document.firstElementChild.scrollHeight - document.firstElementChild.clientHeight;
            player.style.height = window.innerHeight + scrollHeight + 'px';
            rightSection.style.height = window.innerHeight + scrollHeight + 'px';
        }
        else {
            player.style.height = window.innerHeight + 'px';
            rightSection.style.height = window.innerHeight + 'px';
        }
        player.style.width = varWidth + 'px';
    }
}

function playerFullScreen() {
    player.style.height = window.innerHeight + 'px';
    // if scroll needed, add scroll height to player height
    if (!window.innerWidth < window.innerHeight && document.firstElementChild.scrollHeight > document.firstElementChild.clientHeight) {
        const scrollHeight = document.firstElementChild.scrollHeight - document.firstElementChild.clientHeight;
        player.style.height = window.innerHeight + scrollHeight + 'px';
    }
    else {
        player.style.height = window.innerHeight + 'px';
    }
    player.style.width = window.innerWidth + 'px';
}

// Remove scrollbar width if scroll
// function extraSpace() {
//     if (document.firstElementChild.scrollHeight > document.firstElementChild.clientHeight) {
//         document.querySelector('#player-wrapper').style.paddingLeft = window.innerWidth - document.body.offsetWidth + 'px';
//     }
//     else {
//         document.querySelector('#player-wrapper').style.paddingLeft = 0;
//     }
// }


// SECTIONS RESIZE HANDLING
// To do : Rewrite in CSS and remove lines below

// Resize events
function removeSectionsOnresize() {
    if (window.location.hash == '#contribute' || window.location.hash == '#listen') {
        if (window.innerWidth < window.innerHeight) {
            if (form.classList.contains('hide')) {
                form.classList.remove('hide');
            }
            if (!leftSection.classList.contains('max-height')) {
                leftSection.classList.add('max-height');
            }
            if (!rightSection.classList.contains('hide')) {
                rightSection.classList.add('hide');
            }
        }
        else {
            if (form.classList.contains('hide')) {
                form.classList.remove('hide');
            }
            if (leftSection.classList.contains('max-height')) {
                leftSection.classList.remove('max-height');
            }
            if (rightSection.classList.contains('hide')) {
                rightSection.classList.remove('hide');
            }
        }
    }
    else {
        if (window.innerWidth < window.innerHeight) {
            if (!form.classList.contains('hide')) {
                form.classList.add('hide');
            }
            if (leftSection.classList.contains('max-height')) {
                leftSection.classList.remove('max-height');
            }
            if (rightSection.classList.contains('hide')) {
                rightSection.classList.remove('hide');
            }
        }
        else {
            if (form.classList.contains('hide')) {
                form.classList.remove('hide');
            }
            if (!leftSection.classList.contains('max-height')) {
                leftSection.classList.add('max-height');
            }
            if (rightSection.classList.contains('hide')) {
                rightSection.classList.remove('hide');
            }
        }
    }
}

// Click events
function removeSectionsOnclick(button) {
    if (window.innerWidth < window.innerHeight) {
        if (button.dataset.slug == 'contribute' || button.dataset.slug == 'listen') {
            if (form.classList.contains('hide')) {
                form.classList.remove('hide');
            }
            if (!leftSection.classList.contains('max-height')) {
                leftSection.classList.add('max-height');
            }
            if (!rightSection.classList.contains('hide')) {
                rightSection.classList.add('hide');
            }
        }
        else {
            if (!form.classList.contains('hide')) {
                form.classList.add('hide');
            }
            if (leftSection.classList.contains('max-height')) {
                leftSection.classList.remove('max-height');
            }
            if (rightSection.classList.contains('hide')) {
                rightSection.classList.remove('hide');
            }
        }
    }
    else {
        if (form.classList.contains('hide')) {
            form.classList.remove('hide');
        }
        if (leftSection.classList.contains('max-height')) {
            leftSection.classList.remove('max-height');
        }
        if (rightSection.classList.contains('hide')) {
            rightSection.classList.remove('hide');
        }
    }
}
