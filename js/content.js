// Handle file buttons
const iconContainer = document.querySelector('.dropzone');
MutationObserver = window.MutationObserver || window.WebKitMutationObserver;
let observer = new MutationObserver(function(mutations, observer) {
    if (iconContainer.classList.contains('dz-started')) {
        document.querySelector('.dropzone > span').classList.add('hide');
    }
    else {
        document.querySelector('.dropzone > span').classList.remove('hide');
    }
});

observer.observe(iconContainer, {
  attributes: true,
  attributeFilter: ['class']
});


// Remove tooltip on click
window.onclick = () => {
    if (document.querySelector('.tooltip')) {
        document.querySelectorAll('.tooltip').forEach((tooltip) => {
            tooltip.remove();
        });
    }
}

// ON SUBMIT EVENTS
document.querySelector('button[type="submit"]').addEventListener('click', async function(e) {

    document.querySelectorAll('.tooltip').forEach((tooltip) => {
        tooltip.parentElement.removeChild(tooltip);
    });

    // Prevent default submit
    e.preventDefault();
    e.stopPropagation();

    // Pick a random tag from words in message
    const msgValue = document.forms["add_page.participate"]["data[message]"].value;
    if (msgValue) {
        const tags = msgValue.toLowerCase().split(' ');
        const tag = Math.floor(Math.random() * tags.length);
        document.querySelector('input[name="data[tag]"]').setAttribute('value', tags[tag]);
    }

    // Handle required for text inputs
    const textInputs = Array.from(document.querySelectorAll('form input[name="data[name]"], form input[name="data[title]"], form textarea[name="data[message]"]'));
    if (textInputs.some(x => !x.value)) {
        textInputs.forEach((input) => {
            if (!input.value) {
                const tooltip = document.createElement('span');
                if (document.firstElementChild.getAttribute('lang') == 'fr') {
                    tooltip.textContent = 'Veuillez insérer un ' + input.getAttribute('placeholder').toLowerCase() + '.';
                }
                else {
                    tooltip.textContent = 'Please add a ' + input.getAttribute('placeholder').toLowerCase() + '.';
                }
                tooltip.classList.add('tooltip');
                input.parentElement.append(tooltip);
            }
        });
        return
    }

    // Check if website has 'http://' or 'https://'
    const website = document.querySelector('form input[name="data[website]"]');
    let websiteValue = document.forms["add_page.participate"]["data[website]"].value;
    if (websiteValue) {
        if (!websiteValue.startsWith('http://') || !websiteValue.startsWith('https://')) {
            newWebsiteValue = 'http://' + websiteValue;
        }
    }

    // Get files duration
    const file = document.querySelector('.dropzone.files-upload').dropzone.files[0];
    if (file) {
        let path = (window.URL || window.webkitURL).createObjectURL(file);
        const audio = new Audio();
        audio.src = path;
        const duration = await getDuration(audio);
        document.querySelector('input[name="data[duration]"]').setAttribute('value', duration);
    }
    else {
        // Handle required and tooltip for file
        const tooltip = document.createElement('span');
        if (document.firstElementChild.getAttribute('lang') == 'fr') {
            tooltip.textContent = 'Veuillez insérer un fichier .mp3 de 5Mo max.';
        }
        else {
            tooltip.textContent = 'Please add an .mp3 file of max. 5Mb.';
        }
        tooltip.classList.add('tooltip', 'file-margin');
        document.querySelector('.dropzone > span').append(tooltip);
        return
    }

    const progressBar = document.querySelector('.dropzone .dz-preview .dz-progress .dz-upload');
    if (progressBar.style.width !== '100%') {
        const tooltip = document.createElement('span');
        if (document.firstElementChild.getAttribute('lang') == 'fr') {
            tooltip.textContent = 'Le fichier n\'est pas encore uploadé.';
        }
        else {
            tooltip.textContent = 'File isn\'t uploaded yet.';
        }
        tooltip.classList.add('tooltip');
        progressBar.parentElement.parentElement.append(tooltip);
        return
    }

    // Handle required and tooltip for color
    let color = document.forms["add_page.participate"]["data[color]"].value;
    if (color == '#ffffff' || color == '#FFFFFF') {
        const tooltip = document.createElement('span');
        if (document.firstElementChild.getAttribute('lang') == 'fr') {
            tooltip.textContent = 'Sélectionnez une autre couleur.';
        }
        else {
            tooltip.textContent = 'Select another color.';
        }
        tooltip.classList.add('tooltip', 'color-margin');
        document.querySelector('[data-grav-field="color"] .form-input-wrapper').append(tooltip);
        return
    }

    // Handle submit infos pannel
    const submitDiv = document.getElementById('submit-div');
    const selectButtons = submitDiv.querySelectorAll('button:not(.cross)');
    setTimeout(() => {
        submitDiv.classList.remove('hide');
        submitDiv.querySelector('h2').focus();

        // Handle Yes or No click
        selectButtons.forEach((button) =>  {
            button.addEventListener('click', () => {
                if (button == selectButtons[0]) {
                    // Submit
                    document.querySelector('form').submit();
                }
                else {
                    submitDiv.classList.add('hide');
                    document.querySelector('button[data-slug="contribute"]').focus();
                }
            })
        });

        // Close text div on 'x' button click
        submitDiv.querySelector('.cross').addEventListener('click', () => {
                submitDiv.classList.add('hide');
                document.querySelector('button[data-slug="contribute"]').focus();
        });

        // Close text div with ESC
        document.addEventListener("keydown", (e) => {
            if (!submitDiv.classList.contains('hide')) {
                if (e.keyCode === 27) {
                    submitDiv.classList.add('hide');
                    document.querySelector('button[data-slug="contribute"]').focus();
                }
            }
        }, false);

    }, 250);
});


//UTILS

function getDuration(audio) {
    return new Promise(resolve => {
        audio.addEventListener('loadedmetadata', () => {
            const minutes_dec = audio.duration / 60;
            const minutes = Math.floor(minutes_dec);
            const secondes = Math.floor((minutes_dec - minutes) * 60);
            const duration = (minutes < 10 ? '0' : '') + minutes + ':' + (secondes < 10 ? '0' : '') + secondes;
            resolve(duration);
        });
    });
}
