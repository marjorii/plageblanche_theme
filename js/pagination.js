// RIGHT SECTION PAGINATION
const paginationButtons = Array.from(document.querySelectorAll('.pagination'));
const prev = paginationButtons[0];
const next = paginationButtons[1];

paginationButtons.forEach((button) => {
    button.onclick = () => {
        const currentDiv = rightSection.querySelector('div.current');
        // Next button
        if (button == next) {
            // Disable next button if no more content to scroll
            if (currentDiv.scrollLeft >= (currentDiv.scrollWidth - currentDiv.offsetWidth) - currentDiv.offsetWidth) {
                next.setAttribute('disabled', '');
            }
            // Scroll left
            // currentDiv.scrollBy(currentDiv.offsetWidth + 15, 0);
            currentDiv.scrollBy(currentDiv.offsetWidth, 0);

            // Active prev button if left scroll
            if (currentDiv.scrollLeft > 0) {
                prev.removeAttribute('disabled');
            }
        }
        // Prev button
        else {
            // Active next button if content to scroll
            if (currentDiv.scrollLeft < currentDiv.scrollWidth) {
                next.removeAttribute('disabled');
            }
            // Disable prev button if no more content to scroll
            if (currentDiv.scrollLeft <= (currentDiv.offsetWidth)) {
                prev.setAttribute('disabled', '');
            }

            // Scroll right
            // currentDiv.scrollBy(- currentDiv.offsetWidth - 15, 0);
            currentDiv.scrollBy(- currentDiv.offsetWidth, 0);
        }
        // console.log('offsetWidth: ', currentDiv.offsetWidth, 'scrollWidth: ', currentDiv.scrollWidth, 'scrollWidth - offsetWidth: ', currentDiv.scrollWidth - currentDiv.offsetWidth, 'scrollLeft: ', currentDiv.scrollLeft);
    }
});


// Execution in script.js
function paginationSwitch() {
    const currentDiv = rightSection.querySelector('div.current');
    // Remove left scroll if left scroll
    if (currentDiv.scrollLeft > 0) {
        currentDiv.scrollLeft = 0;
    }

    // If scroll, active next button, disable prev button
    if (currentDiv.scrollWidth > currentDiv.offsetWidth) {
        prev.setAttribute('disabled', '');
        next.removeAttribute('disabled');
    }
    // If no scroll, disable buttons
    else {
        paginationButtons.forEach((button) => {
            button.setAttribute('disabled', '');
        });
    }
}
