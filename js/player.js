const playerButtons = document.querySelectorAll('.player-buttons button');
const audio = new Audio();
const pagesPath = Array.from(document.querySelectorAll('.pages-list div')).map((elem) => elem.dataset.src.substring(0, elem.dataset.src.lastIndexOf("/")).replace('/', ''));

let currentPageSlug = null;
if (window.location.pathname.length === 3) {
    currentPageSlug = pagesPath[0];
}
else {
    currentPageSlug = decodeURI(window.location.pathname.replace('/' + document.firstElementChild.getAttribute('lang') + '/', ''));
}

audio.src = '/user/pages/home' + document.querySelector('#player').dataset.src;
playlist();

playerButtons.forEach((button) => {
    button.onclick = () => {

        if (button.dataset.action === 'backward') {
            prevSrc();
        }

        else if (button.dataset.action === 'play') {
            document.querySelectorAll('.player-buttons button[data-action="play"]').forEach((playButton) => {
                let src = playButton.firstElementChild.getAttribute('src');
                if (src.includes('pause')) {
                    src = src.replace('pause', 'play');
                    button.setAttribute('aria-label', 'play');
                    audio.pause();
                }
                else {
                    src = src.replace('play', 'pause');
                    button.setAttribute('aria-label', 'pause');
                    tryToPlayUntilInteraction();
                }
                playButton.firstElementChild.setAttribute('src', src);
            });
        }

        else if (button.dataset.action === 'forward') {
            nextSrc();
        }

        else if (button.dataset.action === 'repeat') {
            let src = button.firstElementChild.getAttribute('src')
            if (button.firstElementChild.src.includes('on')) {
                src = src.replace('-on', '');
                localStorage.setItem('repeat', 'off');
                if (window.location.pathname.includes('fr')) {
                    button.setAttribute('aria-label', 'mode boucle désactivé');
                }
                else {
                    button.setAttribute('aria-label', 'repeat off');
                }
            }
            else {
                src = src.replace('.svg', '-on.svg');
                localStorage.setItem('repeat', 'on');
                if (window.location.pathname.includes('fr')) {
                    button.setAttribute('aria-label', 'mode boucle activé');
                }
                else {
                    button.setAttribute('aria-label', 'repeat on');
                }
            }
            button.firstElementChild.src = src;
            playlist();
        }
    }
});

// Pay attention animation on play button if first vist on website
if (document.referrer.indexOf(window.location.hostname) == -1) {
    document.querySelectorAll('.player-buttons button[data-action="play"]').forEach((button) => {
        button.classList.add('blink');
    });
}
else {
    if (window.innerWidth > window.innerHeight) {
        document.querySelector('.player-buttons#desktop button[data-action="play"]').click();
    }
    else {
        document.querySelector('.player-buttons#mobile button[data-action="play"]').click();
    }
}

// UTILS

function prevSrc() {
    let prevSource = null;
    pagesPath.forEach((path) => {
        if (path.includes(currentPageSlug)) {
            if (pagesPath[pagesPath.indexOf(path) - 1] == undefined) {
                prevSource = pagesPath[pagesPath.length - 1];
            }
            else {
                prevSource = pagesPath[pagesPath.indexOf(path) - 1];
            }
            window.location.href = '/' + document.firstElementChild.getAttribute('lang') + '/' + prevSource + window.location.hash;
        }
    });
}

function nextSrc() {
    let nextSource = null;
    pagesPath.forEach((path) => {
        if (path.includes(currentPageSlug)) {
            if (pagesPath[pagesPath.indexOf(path) + 1] == undefined) {
                nextSource = pagesPath[0];
            }
            else {
                nextSource = pagesPath[pagesPath.indexOf(path) + 1];
            }
            window.location.href = '/' + document.firstElementChild.getAttribute('lang') + '/' + nextSource + window.location.hash;
        }
    });
}

function tryToPlayUntilInteraction() {
    audio.play().catch(() => {
        document.querySelectorAll('.player-buttons button[data-action="play"]').forEach((playButton) => {
            let src = playButton.firstElementChild.getAttribute('src');
            src = src.replace('pause', 'play');
            playButton.firstElementChild.setAttribute('src', src);
        });
        audio.pause();
    });
}

function playlist() {
    if (localStorage && localStorage.getItem('repeat') == 'on') {
        audio.loop = true;
        audio.removeEventListener('ended', nextSrc);

        document.querySelectorAll('.player-buttons button:last-of-type').forEach((button) => {
            if (!button.firstElementChild.src.includes('on')) {
                let src = button.firstElementChild.getAttribute('src');
                src = src.replace('.svg', '-on.svg');
                button.firstElementChild.setAttribute('src', src);
            }
        });
    }
    else {
        audio.loop = false;
        audio.addEventListener('ended', nextSrc);
    }
}
